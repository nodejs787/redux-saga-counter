import React, { Component } from 'react';
import styled from 'styled-components';
import Counter from './components/Counter'

class App extends Component {
  render() {
    return (
      <AppContainer>
	    <Counter />
      </AppContainer>
    );
  }
}

export default App;

const AppContainer = styled.div`
  width: 100%;
  background: #456;
  border: none;
  display: flex;
  flex-direction: column;
  `
